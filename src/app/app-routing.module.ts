import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { RegistrationComponent } from './registration/registration.component';
import { LoginformComponent } from './loginform/loginform.component';
import { UsuarioComponent } from './usuario/usuario.component';

const routes: Routes = [
 
  {
    path: '',
    component: UsuarioComponent
  }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

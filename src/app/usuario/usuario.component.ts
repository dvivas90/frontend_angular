import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import FileSaver from 'file-saver';
import * as XLSX from 'xlsx';

class Usuario {
  constructor(
    public id: string = '',
    public nombre: string = '',
    public primerApellido: string = '',
    public segundoApellido: string = '',
    public genero: string = '',
    public fechaNacimiento: NgbDateStruct = null,
    public nif: string = '',
    public direccion = { calle: '', numero: '', puerta: '', ciudad: '' },
    public tipoUsuario = 'Paciente'
  ) { }
}∫


class Aseguradora {
  constructor(
    public idUsuario: string = '',
    public nombre: string = '',
    public tipoAseguradora: string = '',
    public numeroTarjeta: string = ''
  ) { }
}

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {
  formUsuario: FormGroup;
  @ViewChild('modalClose') modalClose: ElementRef;

  usuarios: Usuario[] = [];
  regModel: Usuario;
  showNew: Boolean = false;
  submitType: string = 'Save';
  selectedRow: number;
  tipoUsuarios: string[] = ['Paciente', 'Profesional'];
  tipoSeguro: string[] = ['Familiar', 'Dental', 'Familiar'];
  tipoProfesional: string[] = ['Medico', 'Enfermero', 'Administrativo'];
  generos: string[] = ['Masculino', 'Femenino'];


  //variables para validaciones
  public tipoUsuarioValido: boolean = true;
  public idValido: boolean = true;
  public nombreValido: boolean = true;
  public primerApellidoValido: boolean = true;

  selectedIndex: number;



  // It maintains list of Usuarios
  aseguradoras: Aseguradora[] = [];


  constructor() {
    // Add default usuario data.
    this.cargarListaUsuarios();


  }

  cargarListaUsuarios() {

    this.usuarios.push(new Usuario('00001', 'Daniel', 'Vivas', 'Granados', 'Masculino', { year: 1992, month: 6, day: 9 }, '123', { calle: '28', numero: '85', puerta: '36', ciudad: 'Popayan' }, 'Profesional'));
    this.usuarios.push(new Usuario('00002', 'Daniel', 'Ricciardo', 'F1', 'Masculino', { year: 1986, month: 8, day: 19 }, '183', { calle: '99', numero: '33', puerta: '22', ciudad: 'Madrid' }, 'Paciente'));
    this.usuarios.push(new Usuario('00003', 'Fernando', 'Alonso', 'F1', 'Masculino', { year: 1990, month: 10, day: 21 }, '986', { calle: '23', numero: '66', puerta: '178', ciudad: 'Valencia' }, 'Paciente'));

  }

  ngOnInit() {
    this.formUsuario = new FormGroup({
      'tipoUsuario': new FormControl(null, Validators.compose([Validators.required, Validators.pattern('.*[^ ].*')])),
      'id': new FormControl(null, Validators.compose([Validators.required, Validators.pattern('.*[^ ].*')])),
      'nombre': new FormControl(null, Validators.compose([Validators.required, Validators.pattern('.*[^ ].*')])),
      'primerApellido': new FormControl(null, Validators.compose([Validators.required, Validators.pattern('.*[^ ].*')])),
      'segundoApellido': new FormControl(null),
      'genero': new FormControl(null),
      'fechaNacimiento': new FormControl(null),
      'nif': new FormControl(null)
    });

  }

  borrarMedicos() {
    this.usuarios = this.usuarios.filter((item: any) => {
      return item.tipoUsuario == 'Profesional'
    });
  }

  validarCampos() {

    if (!this.formUsuario.controls['tipoUsuario'].valid) {
      this.tipoUsuarioValido = false;
    };

    if (!this.formUsuario.controls['id'].valid) {
      this.idValido = false;
    };

    if (!this.formUsuario.controls['nombre'].valid) {
      this.nombreValido = false;
    };

    if (!this.formUsuario.controls['primerApellido'].valid) {
      this.primerApellidoValido = false;
    };

  }

  limpiarValidaciones() {

    this.tipoUsuarioValido = true;
    this.idValido = true;
    this.nombreValido = true;
    this.primerApellidoValido = true;

  }

  // This method associate to New Button.
  onNew() {

    this.formUsuario.reset();
    this.limpiarValidaciones();
    // Initiate new usuario.
    this.regModel = new Usuario();
    // Change submitType to 'Save'.
    this.submitType = 'Save';
    // display usuario entry section.
    this.showNew = true;
  }

  // This method associate to Save Button.
  onSave() {

    this.validarCampos();

    if (this.formUsuario.valid) {
      if (this.submitType === 'Save') {
        // Push usuario model object into usuario list.
        this.usuarios.push(this.regModel);
      } else {
        // Update the existing properties values based on model.
        this.usuarios[this.selectedRow].id = this.regModel.id;
        this.usuarios[this.selectedRow].nombre = this.regModel.nombre;
        this.usuarios[this.selectedRow].primerApellido = this.regModel.primerApellido;
        this.usuarios[this.selectedRow].segundoApellido = this.regModel.segundoApellido;
        this.usuarios[this.selectedRow].genero = this.regModel.genero;
        this.usuarios[this.selectedRow].fechaNacimiento = this.regModel.fechaNacimiento;
        this.usuarios[this.selectedRow].nif = this.regModel.nif;
        this.usuarios[this.selectedRow].direccion = this.regModel.direccion;
        this.usuarios[this.selectedRow].tipoUsuario = this.regModel.tipoUsuario;
      }

      // Hide usuario entry section.
      this.showNew = false;
      this.modalClose.nativeElement.click();
    }
  }

  // This method associate to Edit Button.
  onEdit(index: number) {
    // Assign selected table row index.
    this.selectedRow = index;
    // Initiate new usuario.
    this.regModel = new Usuario();
    // Retrieve selected usuario from list and assign to model.
    this.regModel = Object.assign({}, this.usuarios[this.selectedRow]);
    // Change submitType to Update.
    this.submitType = 'Update';
    // Display usuario entry section.
    this.showNew = true;
  }

  // This method associate to Delete Button.
  onDelete(index: number) {
    // Delete the corresponding usuario entry from the list.
    this.usuarios.splice(index, 1);
  }

  // This method associate to Delete Button.
  confirmDelete(index: number) {
    console.log("AQUI CONFIRMAR ELIMINACION", index);
    this.selectedIndex = index;
  }



  // This method associate toCancel Button.
  onCancel() {
    // Hide usuario entry section.
    this.showNew = false;
  }


  // This method associate to Bootstrap dropdown selection change.
  onChangeTipoUsuario(tipoUsuario: string) {
    // Assign corresponding selected country to model.
    this.regModel.tipoUsuario = tipoUsuario;
  }

  onChangeGenero(genero: string) {
    // Assign corresponding selected country to model.
    this.regModel.genero = genero;
  }



  exportarExcel() {

    /* make the worksheet */
    var ws = XLSX.utils.json_to_sheet(this.usuarios);

    /* add to workbook */
    var wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "People");

    /* write workbook (use type 'binary') */
    var wbout = XLSX.write(wb, { bookType: 'xlsx', type: 'binary' });

    var today = new Date();
    var date = today.getFullYear() + '' + (today.getMonth() + 1) + '' + today.getDate() + '_';
    var time = today.getHours() + "-" + today.getMinutes() + "-" + today.getSeconds();
    var name = "reporteControlMateriales" + date + time;

    FileSaver.saveAs(new Blob([this.s2ab(wbout)], { type: "application/octet-stream" }), name + ".xlsx");
  }

  s2ab(s) {
    var buf = new ArrayBuffer(s.length);
    var view = new Uint8Array(buf);
    for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
    return buf;
  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {
      type: "application/octet-stream"
    });
    var today = new Date();
    var date = today.getFullYear() + '' + (today.getMonth() + 1) + '' + today.getDate() + '_';
    var time = today.getHours() + "-" + today.getMinutes() + "-" + today.getSeconds();
    var name = fileName + date + time;
    FileSaver.saveAs(data, name + ".xlsx");
  }




}

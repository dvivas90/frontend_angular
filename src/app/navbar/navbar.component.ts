import { Component, OnInit } from '@angular/core';
import {DataService} from '../providers/data.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  isCollapsed: Boolean = true;
  constructor(private data:DataService) { }
  ngOnInit() {
    this.data.usuario.nombre='';
  
  }

}
